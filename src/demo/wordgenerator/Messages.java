package demo.wordgenerator;

/**
 * Message constants
 */
public class Messages {
    public static final String USAGE = "Usage: java -jar <jarfile> [-d dictionaryFile] [-f inputFile1 inputFile2 ...]";
    public static final String DICTIONARY_MISSING = "Dictionary not specified";
    public static final String INPUT_MISSING = "Input file(s) not specified";
    public static final String UNKNOWN_PARAMETER = "Unknown parameter\n" + USAGE;
}
