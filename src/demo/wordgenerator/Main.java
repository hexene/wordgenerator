package demo.wordgenerator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * Driver class for generating word-combinations from specified number(s)
 */
public class Main {

    private static final String DEFAULT_DICTIONARY_PATH = "./resources/dict.txt";

    private static String dictionaryPath = DEFAULT_DICTIONARY_PATH;

    private static List<String> filePaths = new LinkedList<>();

    public static void main(String[] args) {

        try {
            parseArguments(args);
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            return;
        }

        // Read numbers from any specified input files
        List<String> numbers = new LinkedList<>();
        for (String filePath : filePaths) {
            try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
                while (true) {
                    String number = reader.readLine();
                    if (number == null) break;
                    numbers.add(number);
                }
            } catch (IOException e) {
                System.err.println("Error opening file: " + filePath);
                return;
            }
        }

        // Construct trie
        Trie trie;
        try {
            trie = Trie.constructTrie(dictionaryPath);
        } catch (IOException e) {
            System.err.println("Error opening dictionary: " + dictionaryPath);
            return;
        }

        // If no files were specified as command-line args, numbers have to be read from STDIN
        if (filePaths.isEmpty()) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
                while (true) {
                    System.out.println("Enter phone number: ");
                    String number = reader.readLine();
                    if (number == null) break;
                    System.out.println();
                    new WordGenerator(number, trie).generate().forEach(System.out::println);
                }
            } catch (IOException e) {
                System.err.println("Error reading from STDIN");
                return;
            }
        } else {
            for (String number : numbers) {
                System.out.println("Number: " + number + "\n");
                new WordGenerator(number, trie).generate().forEach(System.out::println);
            }
        }
    }

    private static void parseArguments(String[] args) throws IllegalArgumentException {
        int size = args.length;
        int i = 0;
        while (i < size) {
            String arg = args[i];
            if (arg.startsWith("-")) {
                if (arg.equals("-d")) {
                    if (++i < size && !args[i].startsWith("-")) {
                        dictionaryPath = args[i];
                        i++;
                    } else {
                        throw new IllegalArgumentException(Messages.DICTIONARY_MISSING);
                    }
                } else if (arg.equals("-f")) {
                    while (++i < size && !args[i].startsWith("-"))
                        filePaths.add(args[i]);
                    if (filePaths.isEmpty())
                        throw new IllegalArgumentException(Messages.INPUT_MISSING);
                } else if (arg.equals("--help")) {
                    // Not ideal as this is not an illegal argument, but throwing an exception is simpler
                    throw new IllegalArgumentException(Messages.USAGE);
                } else {
                    throw new IllegalArgumentException(Messages.UNKNOWN_PARAMETER);
                }
            } else {
                throw new IllegalArgumentException(Messages.UNKNOWN_PARAMETER);
            }
        }
    }
}
