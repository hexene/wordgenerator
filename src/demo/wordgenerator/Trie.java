package demo.wordgenerator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Representation of a Trie
 */
public class Trie {

    private static final int ALPHABET_COUNT = 26;

    /**
     * Representation of a single node in the trie
     */
    private static class TrieNode {
        final TrieNode[] nextCharacters;
        boolean endOfWord;

        public TrieNode() {
            this.nextCharacters = new TrieNode[ALPHABET_COUNT];
        }

        public TrieNode[] getNextCharacters() {
            return nextCharacters;
        }

        public boolean isEndOfWord() {
            return endOfWord;
        }

        public void setEndOfWord(boolean endOfWord) {
            this.endOfWord = endOfWord;
        }
    }

    /**
     * Constructs a trie from the contents of the file specified
     *
     * @param file file to construct trie from
     * @return trie object
     * @throws IOException
     */
    public static Trie constructTrie(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            Trie trie = new Trie();
            while (true) {
                String word = reader.readLine();
                if (word == null) break;
                trie.insert(word);
            }
            return trie;
        }
    }

    private final TrieNode rootNode;

    private int count;

    private Trie() {
        rootNode = new TrieNode();
    }

    /**
     * Inserts a word into the trie
     *
     * @param word word to insert
     * @return true if successful, false otherwise
     */
    private boolean insert(String word) {
        if (word == null)
            return false;
        String upperCaseWord = word.toUpperCase().replaceAll("[^A-Z]", "");
        if (upperCaseWord.isEmpty())
            return false;

        TrieNode currentNode = rootNode;
        int size = upperCaseWord.length();
        for (int i = 0; i < size; i++) {
            char currentCharacter = upperCaseWord.charAt(i);
            int charOffset = currentCharacter - 'A';
            TrieNode[] nextCharacters = currentNode.getNextCharacters();
            TrieNode nextNode = nextCharacters[charOffset];
            if (nextNode == null) {
                nextNode = new TrieNode();
                nextCharacters[charOffset] = nextNode;
            }
            currentNode = nextNode;
        }
        currentNode.setEndOfWord(true);
        count++;
        return true;
    }

    /**
     * Fetches the count of words stored in the trie
     *
     * @return count
     */
    public int getCount() {
        return count;
    }

    /**
     * Searches for a word in the trie
     *
     * @param word word to search for
     * @return true if found, false otherwise
     */
    public boolean search(String word) {
        if (word == null)
            return false;
        String upperCaseWord = word.toUpperCase().replaceAll("[^A-Z]", "");
        if (upperCaseWord.isEmpty())
            return false;

        TrieNode currentNode = rootNode;
        int size = upperCaseWord.length();
        for (int i = 0; i < size; i++) {
            char currentCharacter = upperCaseWord.charAt(i);
            int charOffset = currentCharacter - 'A';
            TrieNode nextNode = currentNode.getNextCharacters()[charOffset];
            if (nextNode == null) {
                return false;
            }
            currentNode = nextNode;
        }
        return currentNode.isEndOfWord();
    }

    /**
     * Constructs a table for DP for the character mappings specified
     * Eg.:
     * If the trie contains the words "D", "DO" and "DOG", for the input {{'D', 'E', 'F'}, {'M', 'N', 'O'}, {'G', 'H', 'I'}},
     * the returned table will be:
     * {{DOG}, <- list of matched 3 character words
     *  {DO},  <- list of matched 2 character words
     *  {D}}   <- list of matched 1 character words
     *
     * @param characterMappings character mappings to construct the DP table for
     * @return DP table
     */
    public List<List<String>> lookupWords(List<List<Character>> characterMappings) {
        int size = characterMappings.size();
        if (size == 0)
            return Collections.emptyList();

        List<List<String>> words = new ArrayList<>(size);
        IntStream.range(0, size).forEach(i -> words.add(new LinkedList<>()));

        Deque<StackEntry> stack = new ArrayDeque<>(size);

        // Generating word combinations from the character mappings and matching them with words in the trie
        // are done together here to save time
        int level = 0;
        String partialWord = "";
        stack.push(new StackEntry(characterMappings.get(level).iterator(), rootNode));
        while (!stack.isEmpty()) {
            StackEntry stackEntry = stack.peek();
            Iterator<Character> currentIterator = stackEntry.getIterator();
            if (!currentIterator.hasNext()) {
                stack.pop();
                if (partialWord.length() > 0)
                    partialWord = partialWord.substring(0, partialWord.length() - 1);
                level--;
                continue;
            }
            char currentChar = currentIterator.next();
            TrieNode previousNode = stackEntry.getTrieNode();
            TrieNode currentNode = previousNode.getNextCharacters()[currentChar - 'A'];
            if (currentNode == null) {
                continue;
            } else {
                partialWord += currentChar;
                if (currentNode.isEndOfWord())
                    words.get(size - partialWord.length()).add(partialWord);
                if (level < size - 1)
                    stack.push(new StackEntry(characterMappings.get(++level).iterator(), currentNode));
                else if (partialWord.length() > 0)
                    partialWord = partialWord.substring(0, partialWord.length() - 1);
            }
        }
        return words;
    }

    /**
     * Container for objects to be stored in the stack during DP table construction
     */
    private static class StackEntry {
        final Iterator<Character> iterator;
        final TrieNode trieNode;

        private StackEntry(Iterator<Character> iterator, TrieNode trieNode) {
            this.iterator = iterator;
            this.trieNode = trieNode;
        }

        public Iterator<Character> getIterator() {
            return iterator;
        }

        public TrieNode getTrieNode() {
            return trieNode;
        }
    }
}
