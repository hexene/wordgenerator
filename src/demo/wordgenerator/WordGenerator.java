package demo.wordgenerator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Word-combination generator
 */
public class WordGenerator {

    private static final Map<Character, List<Character>> CHARACTER_MAPPINGS = new HashMap<>();

    static {
        CHARACTER_MAPPINGS.put('0', Collections.emptyList());
        CHARACTER_MAPPINGS.put('1', Collections.emptyList());
        CHARACTER_MAPPINGS.put('2', Arrays.asList('A', 'B', 'C'));
        CHARACTER_MAPPINGS.put('3', Arrays.asList('D', 'E', 'F'));
        CHARACTER_MAPPINGS.put('4', Arrays.asList('G', 'H', 'I'));
        CHARACTER_MAPPINGS.put('5', Arrays.asList('J', 'K', 'L'));
        CHARACTER_MAPPINGS.put('6', Arrays.asList('M', 'N', 'O'));
        CHARACTER_MAPPINGS.put('7', Arrays.asList('P', 'Q', 'R', 'S'));
        CHARACTER_MAPPINGS.put('8', Arrays.asList('T', 'U', 'V'));
        CHARACTER_MAPPINGS.put('9', Arrays.asList('W', 'X', 'Y', 'Z'));
    }

    private final String number;
    private final Trie trie;

    /**
     * Initializes the generator
     *
     * @param number number to generate words from
     * @param trie trie to match words with
     */
    public WordGenerator(String number, Trie trie) {
        this.number = number.replaceAll("[^0-9]", "");
        this.trie = trie;
    }

    /**
     * Generates all possible word-combinations for the specified number
     *
     * @return list of generated word-combinations
     */
    public List<String> generate() {
        return generateWordCombinations(constructDPTable());
    }

    private List<List<List<String>>> constructDPTable() {
        int size = number.length();
        if (size == 0)
            return Collections.emptyList();

        int zeroOrOneIndex = Integer.MIN_VALUE;
        List<List<Character>> characterMappings = new ArrayList<>(size);
        for (int i = 0; i < number.length(); i++) {
            char currentCharacter = number.charAt(i);
            if (currentCharacter == '0' || currentCharacter == '1') {
                // Early optimisation: if consecutive 0/1s are found, skip this number as per the problem specification
                if (zeroOrOneIndex == i - 1)
                    return Collections.emptyList();
                else
                    zeroOrOneIndex = i;
            }
            List<Character> characterMapping = CHARACTER_MAPPINGS.get(currentCharacter);
            characterMappings.add(characterMapping);
        }
        // Construct the complete DP table for the current number
        List<List<List<String>>> dpTable = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            List<List<String>> partialWordsList = trie.lookupWords(characterMappings.subList(i, size));
            // If no single character words are found for the current mapping level, we insert the digit itself instead, as this will make
            // using digits in the word-combinations easier later
            List<String> partialWords = partialWordsList.get(size - i - 1);
            if (partialWords.isEmpty())
                partialWords.add(String.valueOf(number.charAt(i)));
            dpTable.add(partialWordsList);
        }
        return dpTable;
    }

    private List<String> generateWordCombinations(List<List<List<String>>> words) {
        int size = words.size();
        if (size == 0)
            return Collections.emptyList();

        List<String> finalResults = new LinkedList<>();
        List<String> intermediateResults = new LinkedList<>();

        Deque<StackEntry> stack = new ArrayDeque<>(size);
        stack.push(new StackEntry(words.get(0).iterator(), new Position(), Arrays.asList("")));

        while (!stack.isEmpty()) {
            StackEntry stackEntry = stack.peek();
            Iterator<List<String>> currentIterator = stackEntry.getIterator();
            if (!currentIterator.hasNext()) {
                stack.pop();
                if (!stack.isEmpty())
                    stack.peek().getPosition().increment();
                continue;
            }
            Position position = stackEntry.getPosition();
            List<String> rightPartialWords = currentIterator.next();
            if (rightPartialWords.isEmpty()) {
                position.increment();
                continue;
            }
            for (String leftPartialWord : stackEntry.getPartialWords()) {
                for (String rightPartialWord : rightPartialWords) {
                    if (!leftPartialWord.isEmpty()) {
                        // Ignore combinations with consecutive digits
                        if (!Character.isDigit(leftPartialWord.charAt(leftPartialWord.length() - 1)) ||
                                !Character.isDigit(rightPartialWord.charAt(0)))
                            intermediateResults.add(leftPartialWord + "-" + rightPartialWord);
                    } else {
                        intermediateResults.add(rightPartialWord);
                    }
                }
            }
            if (position.get() > 0) {
                stack.push(new StackEntry(words.get(size - position.get()).iterator(), new Position(), intermediateResults));
            } else {
                position.increment();
                finalResults.addAll(intermediateResults);
            }
            intermediateResults = new LinkedList<>();
        }
        return finalResults;
    }

    /**
     * A mutable integer container to keep track of the iterator position in the DP table
     */
    private static class Position {
        private int value;

        public int get() {
            return value;
        }

        public void increment() {
            value++;
        }
    }

    /**
     * Container for objects to be stored in the stack during DP table lookup
     */
    private static class StackEntry {
        final Iterator<List<String>> iterator;
        final Position position;
        final List<String> partialWords;

        private StackEntry(Iterator<List<String>> iterator, Position position, List<String> partialWords) {
            this.iterator = iterator;
            this.position = position;
            this.partialWords = partialWords;
        }

        public Iterator<List<String>> getIterator() {
            return iterator;
        }

        public Position getPosition() {
            return position;
        }

        public List<String> getPartialWords() {
            return partialWords;
        }
    }
}
