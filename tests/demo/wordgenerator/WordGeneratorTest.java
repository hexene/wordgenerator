package demo.wordgenerator;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WordGeneratorTest {

    @Test
    public void generate() throws Exception {
        Trie trie = Trie.constructTrie(TestConstants.DICTIONARY_PATH);

        List<String> words = new WordGenerator("", trie).generate();
        assertTrue(words.isEmpty());

        // Number contains 2 consecutive digits (0, 1) which cannot be converted
        words = new WordGenerator("301", trie).generate();
        assertTrue(words.isEmpty());

        words = new WordGenerator("364", trie).generate();
        assertEquals(2, words.size());
        assertTrue(words.contains("DOG"));
        assertTrue(words.contains("DO-4"));
    }

}