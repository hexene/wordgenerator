package demo.wordgenerator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MainTest {

    private final ByteArrayInputStream inputStream = new ByteArrayInputStream("364".getBytes());
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errorStream = new ByteArrayOutputStream();

    @Before
    public void setUp() throws Exception {
        System.setIn(inputStream);
        System.setOut(new PrintStream(outputStream));
        System.setErr(new PrintStream(errorStream));
    }

    @After
    public void tearDown() throws Exception {
        System.setIn(null);
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void main() throws Exception {
        Main.main(new String[] {"-xyz"});
        assertTrue(errorStream.toString().contains(Messages.UNKNOWN_PARAMETER));
        errorStream.reset();

        Main.main(new String[] {"xyz"});
        assertTrue(errorStream.toString().contains(Messages.UNKNOWN_PARAMETER));
        errorStream.reset();

        Main.main(new String[] {"-d"});
        assertTrue(errorStream.toString().contains(Messages.DICTIONARY_MISSING));
        errorStream.reset();

        Main.main(new String[] {"-f"});
        assertTrue(errorStream.toString().contains(Messages.INPUT_MISSING));
        errorStream.reset();

        Main.main(new String[] {"--help"});
        assertTrue(errorStream.toString().contains(Messages.USAGE));
        errorStream.reset();

        // Input (364) read from stdin, dictionary file read from the command-line
        Main.main(new String[] {"-d", TestConstants.DICTIONARY_PATH});
        assertEquals(0, errorStream.size());
        assertTrue(outputStream.toString().contains("DOG"));
        assertTrue(outputStream.toString().contains("DO-4"));
        outputStream.reset();

        // Input and dictionary file read from the command-line
        Main.main(new String[] {"-f", TestConstants.INPUT_FILE_PATH, "-d", TestConstants.DICTIONARY_PATH});
        assertEquals(0, errorStream.size());
        assertTrue(outputStream.toString().contains("DOG"));
        assertTrue(outputStream.toString().contains("DO-4"));
        outputStream.reset();
    }
}