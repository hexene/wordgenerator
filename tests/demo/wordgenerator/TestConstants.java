package demo.wordgenerator;

public class TestConstants {
    public static final String DICTIONARY_PATH = "./resources/small_dict.txt";
    public static final String INPUT_FILE_PATH = "./resources/input.txt";
}
