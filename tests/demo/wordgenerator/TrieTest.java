package demo.wordgenerator;

import org.junit.Before;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.RandomAccess;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TrieTest {

    private Trie trie;

    @Before
    public void setUp() throws Exception {
        trie = Trie.constructTrie(TestConstants.DICTIONARY_PATH);
    }

    @org.junit.Test
    public void constructTrie() throws Exception {
        // NOTE: We don't actually construct the trie here since it's required for all tests in this class
        assertEquals(3, trie.getCount());
        try (BufferedReader reader = new BufferedReader(new FileReader(TestConstants.DICTIONARY_PATH))) {
            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                assertTrue(trie.search(line));
            }
        }
    }

    @org.junit.Test
    public void lookupWords() throws Exception {
        List<List<String>> words = trie.lookupWords(
                Arrays.asList(
                        Arrays.asList('D', 'E', 'F'),
                        Arrays.asList('M', 'N', 'O'),
                        Arrays.asList('G', 'H', 'I')));

        // This is required for constant-time index based lookup
        assertTrue(words instanceof RandomAccess);

        assertEquals(3, words.size());
        assertEquals(1, words.get(0).size());
        assertEquals("DOG", words.get(0).get(0));
        assertEquals(1, words.get(1).size());
        assertEquals("DO", words.get(1).get(0));
        assertEquals(1, words.get(2).size());
        assertEquals("D", words.get(2).get(0));
    }
}